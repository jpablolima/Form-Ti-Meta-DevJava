
-- Crie comandos SQL para inserir os dados apresentados a seguir:
--
-- Tabela TIPO:
-- 1, Banco de dados
-- 2, Programação
-- 3, Modelagem de dados

-- Tabela INSTRUTOR:
-- 1, Clovis Eulalio, 1111-1111
-- 2, Fabio Perucello, 1212-1212

-- Tabela CURSO:
-- 1, Java Fundamentos, 2, 2, 270
-- 2, Java Avançado, 2, 2, 330
-- 3, SQL Completo, 1, 1, 170
-- 4, Logicas, 2, 1, 270




-- Tabela ALUNO:
-- 1, José, Rua XV de Novembro 72, jose@evolua.com.br
-- 2, Wagner, Av. Paulista, wagner@evolua.com.br
-- 3, Emílio, Rua Lajes 103, ap: 701, emilio@evolua.com.br
-- 4, Cris, Rua Tauney 22, cris@evolua.com.br
-- 5, Regina, Rua Salles 305, regina@sevolua.com.br
-- 6, Fernando, Av. Central 30, fernando@evolua.com.br
--



CREATE  DATABASE escolaEvolua2 DEFAULT CHARSET=latin1;
use escolaEvolua2;


CREATE TABLE TIPO(
    CODIGO INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    TIPO VARCHAR (40) NOT NULL,
    PRIMARY KEY(CODIGO)
);


-- inserindo dados na tabela tipo

INSERT INTO `escolaEvolua2`.`tipo` (`tipo`) VALUES ('Banco de Dados');
INSERT INTO `escolaEvolua2`.`tipo` (`tipo`) VALUES ('Programação');
INSERT INTO `escolaEvolua2`.`tipo` (`tipo`) VALUES ('Modelagem de Dados');




CREATE TABLE INSTRUTOR(
    CODIGO INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    INSTRUTOR VARCHAR (70) NOT NULL,
    TELEFONE VARCHAR (9) NULL,
    PRIMARY KEY(CODIGO)
);

-- inserindo dados na tabela Instrutor

-- Tabela INSTRUTOR:
-- 1, Clovis Eulalio, 1111-1111
-- 2, Fabio Perucello, 1212-1212


INSERT INTO `escolaEvolua2`.`instrutor` (`instrutor`, `telefone`) VALUES ('Clovis Eulalio', '1111-1111');
INSERT INTO `escolaEvolua2`.`instrutor` (`instrutor`, `telefone`) VALUES ('Fabio Perucello', '1212-1212');




-- Tabela CURSO:
-- 1, Java Fundamentos, 2, 2, 270
-- 2, Java Avançado, 2, 2, 330
-- 3, SQL Completo, 1, 1, 170
-- 4, Logicas, 2, 1, 270

-- inserindo dados na tabela Instrutor


CREATE TABLE CURSO (
CODIGO INTEGER UNSIGNED NOT NULL AUTO_INCREMENT, 
CURSO VARCHAR(64) NOT NULL, 
TIPO INTEGER UNSIGNED NOT NULL,
INSTRUTOR INTEGER UNSIGNED NOT NULL, 
VALOR DOUBLE NOT NULL, 
PRIMARY KEY(CODIGO), 
INDEX FK_TIPO(TIPO), 
INDEX FK_INSTRUTOR(INSTRUTOR), 
FOREIGN KEY(TIPO) REFERENCES TIPO(CODIGO),
FOREIGN KEY(INSTRUTOR) REFERENCES INSTRUTOR(CODIGO) 
);

-- Tabela CURSO:
-- 1, Java Fundamentos, 2, 2, 270
-- 2, Java Avançado, 2, 2, 330
-- 3, SQL Completo, 1, 1, 170
-- 4, Logicas, 2, 1, 270

-- inserindo dados na tabela Instrutor


INSERT INTO `escolaEvolua2`.`CURSO` (`CURSO`, `tipo`, ``) VALUES ('Clovis Eulalio', '1111-1111');




CREATE TABLE ALUNO (
CODIGO INTEGER UNSIGNED NOT NULL AUTO_INCREMENT, 
ALUNO VARCHAR(64) NOT NULL, 
ENDERECO VARCHAR(230) NOT NULL, 
EMAIL VARCHAR(128) NOT NULL, 
PRIMARY KEY(CODIGO) 
);

CREATE TABLE PEDIDO (
CODIGO INTEGER UNSIGNED NOT NULL AUTO_INCREMENT, 
ALUNO INTEGER UNSIGNED NOT NULL, 
DATAHORA DATETIME NOT NULL, 
PRIMARY KEY(CODIGO), 
INDEX FK_ALUNO(ALUNO), 
FOREIGN KEY(ALUNO) REFERENCES ALUNO(CODIGO) 
);



CREATE TABLE PEDIDO_DETALHE (
PEDIDO INTEGER UNSIGNED NOT NULL, 
CURSO INTEGER UNSIGNED NOT NULL,
VALOR DOUBLE NOT NULL, 
INDEX FK_PEDIDO(PEDIDO),
INDEX FK_CURSO(CURSO), 
PRIMARY KEY(PEDIDO, CURSO), 
FOREIGN KEY(PEDIDO) REFERENCES PEDIDO(CODIGO), 
FOREIGN KEY(CURSO) REFERENCES CURSO(CODIGO) 
);


-- COMANDOS INSERT 


-- 1, Banco de dados
-- 2, Programação
-- 3, Modelagem de dados


